require 'json'
require 'nokogiri'
require './lib/s3net-http'

unless bucket_name = ARGV[0]
  puts "Usage: #{File.basename(__FILE__)} <BUCKET_NAME>"
  exit 1
end

def random_string n
  chars = ('a'..'z').to_a + ('A'..'Z').to_a + ('0'..'9').to_a
  Array.new(n){chars[rand(chars.size)]}.join
end

def request
  response = yield
  case response.code.to_s
  when /20\d/ then p response.inspect
  else puts "#{response.inspect}\n#{response.body}"; raise response.inspect
  end
  response
end

def upload_part s3request, upload_id, number, object_name, data, headers
  response = request do
    s3request.upload_part object_name, data, number, upload_id, headers
  end
  { :etag => response['etag'], :number => number }
end

s3request   = S3NetHttp.new bucket_name
object_name = random_string 16
headers     = { 'content-type' => 'text/plain' }

puts "\nInitiateMultipartUpload"
response = request do
  s3request.initiate_multipart_upload(object_name, headers.merge({
    'x-amz-acl'           => :private.to_s,
    'x-amz-storage-class' => :reduced_redundancy.to_s.upcase,
  }))
end
xml = Nokogiri::XML response.body
upload_id = xml.root.children.find {|c| c.name =='UploadId' }.child.text

puts "\nUploadPart"
parts = []
parts << upload_part(s3request, upload_id, 1, object_name, 'a' * 5*1024*1024, headers)
parts << upload_part(s3request, upload_id, 2, object_name, 'b' * 5*1024*1024, headers)

puts "\nCompleteMultipartUpload"
request { s3request.complete_multipart_upload object_name, parts, upload_id, headers }

puts "\n(press any key to delete the object)"
$stdin.getc
request { s3request.delete object_name }
