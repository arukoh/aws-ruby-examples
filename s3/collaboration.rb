require 'mime/types'
require './lib/s3sdk'
require './lib/s3net-http'

(bucket_name, file_name, metadata) = ARGV
unless bucket_name && file_name
  puts "Usage: #{File.basename(__FILE__)} <BUCKET_NAME> <FILE_NAME>"
  exit 1
end

content_type = MIME::Types.type_for(file_name)[0]
S3SDK.new(bucket_name).upload_and_delete(file_name) do |object|
  upload = object.multipart_upload(
    :content_type  => content_type,
    :acl           => :private,
    :storage_class => :reduced_redundancy,
    :metadata      => { :data => (JSON[File.read(metadata)] if metadata) },
  )
  id = upload.id
  
  s3request = S3NetHttp.new bucket_name
  response = s3request.upload_part file_name, File.read(file_name), 1, id, {
    'content-type'   => content_type.to_s,
#    'content-length' => File.size(file_name).to_s,
  }
  case response.code.to_s
  when /20\d/ then p response.inspect
  else puts "#{response.inspect}\n#{response.body}"; exit 1
  end

  upload = object.multipart_uploads[id]
  upload.complete(:remote_parts)

  puts "Uploaded #{file_name} to: #{object.public_url}"
  puts "\nUse this URL to download the file: #{object.url_for(:read)}"
end
