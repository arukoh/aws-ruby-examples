require 'mime/types'
require './lib/s3sdk'

(bucket_name, file_name, metadata) = ARGV
unless bucket_name && file_name
  puts "Usage: #{File.basename(__FILE__)} <BUCKET_NAME> <FILE_NAME>"
  exit 1
end

S3SDK.new(bucket_name).upload_and_delete(file_name) do |object|
  object.write(:file          => file_name,
               :content_type  => MIME::Types.type_for(file_name)[0],
               :acl           => :private,
               :storage_class => :reduced_redundancy,
               :metadata      => { :data => (JSON[File.read(metadata)] if metadata) },
              )
  puts "Uploaded #{file_name} to: #{object.public_url}"
  puts "\nUse this URL to download the file: #{object.url_for(:read)}"
end
