require 'json'
require 'mime/types'
require './lib/s3net-http'

(bucket_name, file_name, metadata) = ARGV
unless bucket_name && file_name && File.exist?(file_name)
  puts "Usage: #{File.basename(__FILE__)} <BUCKET_NAME> <FILE_NAME>"
  exit 1
end

def request
  response = yield
  case response.code.to_s
  when /20\d/ then p response.inspect
  else puts "#{response.inspect}\n#{response.body}"; raise response.inspect
  end
  response
end

s3request = S3NetHttp.new bucket_name

puts "\nUpload"
request do
  s3request.upload(file_name, File.read(file_name), {
    'content-type'        => MIME::Types.type_for(file_name)[0].to_s,
#    'content-length'      => File.size(file_name),
    'x-amz-acl'           => :private.to_s,
    'x-amz-storage-class' => :reduced_redundancy.to_s.upcase,
    'x-amz-meta-data'     => (JSON[File.read(metadata)] if metadata).to_s
  })
end

puts "\n(press any key to delete the object)"
$stdin.getc
request { s3request.delete file_name }
