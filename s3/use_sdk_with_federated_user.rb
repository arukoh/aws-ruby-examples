require 'logger'
require 'aws'
require 'uuidtools'

(bucket_name, file_name) = ARGV
unless bucket_name && file_name
  puts "Usage: #{File.basename(__FILE__)} <BUCKET_NAME> <FILE_NAME>"
  exit 1
end

def upload_policy(bucket, key_prefix)
  AWS::STS::Policy.new do |policy|
    policy.allow( :actions => ['s3:PutObject'], :resources => "arn:aws:s3:::#{bucket}/#{key_prefix}/*")
  end
end

def upload_session(name, policy, duration = 60*60)
  AWS::STS.new.new_federated_session(name, :policy => policy, :duration => duration)
end

def bucket(name, credentials = nil)
  options = credentials ? {
    :access_key_id     => credentials[:access_key_id],
    :secret_access_key => credentials[:secret_access_key],
    :session_token     => credentials[:session_token],
  } : {}
  bucket = AWS::S3.new(options).buckets[name]
  raise 'Bucket not exists' unless bucket.exists?
  bucket
end

AWS.config(
#  :access_key_id     => ENV['ACCESS_KEY_ID'],
#  :secret_access_key => ENV['AWS_SECRET_ACCESS_KEY']
  :s3_endpoint   => 's3-ap-northeast-1.amazonaws.com',
  :logger        => Logger.new($stdout),
#  :log_level     => :debug,
#  :log_formatter => AWS::Core::LogFormatter.debug,
  :proxy_uri     => ENV['HTTPS_PROXY'] || ENV['https_proxy']
)
object_key_prefix = UUIDTools::UUID.timestamp_create.to_s

# get bucket that use temporary credentials
session = upload_session('TemporaryUser', upload_policy(bucket_name, object_key_prefix))
bucket = bucket(bucket_name, session.credentials)

# should raise error of access denied when unauthorized object key
object_key = File.basename(file_name)
begin
  bucket.objects[object_key].write(:file => file_name)
rescue AWS::S3::Errors::AccessDenied => e
  # continue
end

# should upload
object_key = "#{object_key_prefix}/#{File.basename(file_name)}"
object = bucket.objects[object_key].write(:file => file_name)

# change credentials
object = bucket(bucket_name).objects[object.key]

# delete created object
if object.exists?
  puts "\n(press any key to delete the object)"
  $stdin.getc
  object.delete
end
