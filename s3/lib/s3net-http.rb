require 'net/https'
require File.expand_path('authorization', File.dirname(__FILE__))

class S3NetHttp
  attr_reader :bucket_name
  ENDPOINT_AP_N1 = 's3-ap-northeast-1.amazonaws.com'

  def initialize bucket_name
    @bucket_name = bucket_name
  end

  def upload object_name, data, headers = {}
    request object_name do |uri, https|
      https.put uri.path, data, authorized_headers(:put, uri, headers)
    end
  end

  def initiate_multipart_upload object_name, headers = {}
    request object_name, 'uploads' do |uri, https|
      https.post "#{uri.path}?#{uri.query}", nil, authorized_headers(:post, uri, headers)
    end
  end

  def upload_part object_name, data, part_number, upload_id, headers = {}
    request object_name, "partNumber=#{part_number}&uploadId=#{upload_id}" do |uri, https|
      https.put "#{uri.path}?#{uri.query}", data, authorized_headers(:put, uri, headers)
    end
  end

  def complete_multipart_upload object_name, parts, upload_id, headers = {}
    data = "<CompleteMultipartUpload>\n"
    parts.each do |part|
      data += <<XML
  <Part>
    <PartNumber>#{part[:number]}</PartNumber>
    <ETag>#{part[:etag]}</ETag>
  </Part>
XML
    end
    data += "</CompleteMultipartUpload>"
    request object_name, "uploadId=#{upload_id}" do |uri, https|
      https.post "#{uri.path}?#{uri.query}", data, authorized_headers(:post, uri, headers)
    end
  end

  def delete object_name
    request object_name do |uri, https|
      https.delete uri.path, authorized_headers(:delete, uri)
    end
  end

  private

  def authorized_headers method, uri, headers = {}
    Authorization.new(method, uri, headers).authorized_headers
  end

  def request path, query = nil
    uri = URI.parse "https://#{bucket_name}.#{ENDPOINT_AP_N1}:443/#{path}#{"?#{query}" if query}"
    https = http_class.new uri.host, uri.port
    https.use_ssl = true
#    https.ca_file = '/usr/share/ssl/cert.pem'
#    https.verify_mode = OpenSSL::SSL::VERIFY_PEER
    https.start { yield uri, https }
  end

  def http_class
    if http_proxy = proxy_uri
      uri = URI.parse http_proxy
      proxy_user, proxy_pass = uri.userinfo.split(/:/) if uri.userinfo
      Net::HTTP::Proxy(uri.host, uri.port, proxy_user, proxy_pass)
    else
      Net::HTTP
    end
  end

  def proxy_uri
    [ENV['HTTPS_PROXY'],ENV['https_proxy'],ENV['HTTP_PROXY'],ENV['http_proxy']].compact.uniq[0]
  end
end
