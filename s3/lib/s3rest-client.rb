require 'rest-client'
require File.expand_path('authorization', File.dirname(__FILE__))

class S3RestClient
  attr_reader :bucket_name
  ENDPOINT_AP_N1 = 's3-ap-northeast-1.amazonaws.com'

  def initialize bucket_name
    RestClient.log   = $stdout
    RestClient.proxy = proxy_uri

    @bucket_name = bucket_name
  end

  def upload object_name, data, headers = {}
    put_request uri(object_name), data, headers
  end

  def initiate_multipart_upload object_name, headers = {}
    uri = uri File.basename(object_name), "uploads"
    RestClient.post uri, nil, authorized_headers(:post, uri, headers)
  end

  def upload_part object_name, data, part_number, upload_id, headers = {}
    query = "partNumber=#{part_number}&uploadId=#{upload_id}"
    put_request uri(object_name, query), data, headers
  end

  def complete_multipart_upload object_name, parts, upload_id, headers = {}
    uri = uri File.basename(object_name), "uploadId=#{upload_id}"
    data = "<CompleteMultipartUpload>\n"
    parts.each do |part|
      data += <<XML
  <Part>
    <PartNumber>#{part[:number]}</PartNumber>
    <ETag>#{part[:etag]}</ETag>
  </Part>
XML
    end
    data += "</CompleteMultipartUpload>"
    RestClient.post uri, data, authorized_headers(:post, uri, headers)
  end

  def delete object_name
    uri = uri File.basename(object_name)
    headers = authorized_headers :delete, uri
    RestClient.delete uri, headers
  end

  private

  def uri object_name, query = nil
    parts = [ "https://#{bucket_name}.#{ENDPOINT_AP_N1}:443" ]
    parts << "/#{object_name}"
    parts << "?#{query}" if query
    parts.join
  end

  def authorized_headers method, uri, headers = {}
    Authorization.new(method, uri, headers).authorized_headers
  end

  def put_request uri, data, headers
    RestClient.put uri, data, authorized_headers(:put, uri, headers)
  end

  def proxy_uri
    [ENV['HTTPS_PROXY'],ENV['https_proxy'],ENV['HTTP_PROXY'],ENV['http_proxy']].compact.uniq[0]
  end
end
