require 'uri'
require 'time'
require 'base64'
require 'digest/md5'
require 'openssl'

class Authorization

  def initialize http_method, uri, headers,
    access_key_id = ENV['AWS_ACCESS_KEY_ID'], secret_access_key = ENV['AWS_SECRET_ACCESS_KEY']

    @http_method       = http_method
    @uri               = URI == uri ? uri : URI.parse(uri.to_s)
    @headers           = headers
    @access_key_id     = access_key_id
    @secret_access_key = secret_access_key
  end

  def authorized_headers
    signature = sign(secret_access_key, string_to_sign, 'sha1')
    headers['authorization'] = "AWS #{access_key_id}:#{URI.escape(signature)}"
    headers
  end

  private
  attr_reader :http_method, :uri, :headers, :access_key_id, :secret_access_key

  def sign secret, string_to_sign, digest_method = 'sha256'
    Base64.encode64(hmac(secret, string_to_sign, digest_method)).strip
  end

  def hmac key, value, digest = 'sha256'
    OpenSSL::HMAC.digest(OpenSSL::Digest::Digest.new(digest), key, value)
  end

  def string_to_sign
    [
      http_method.to_s.upcase,
      headers.values_at('content-md5', 'content-type').join("\n"),
      signing_string_date,
      canonicalized_headers,
      canonicalized_resource,
    ].flatten.compact.join("\n")
  end

 def signing_string_date
    headers.detect{|k,v| k.to_s =~ /^x-amz-date$/i } ? '' : headers['date'] ||= Time.now.rfc822
  end

  def canonicalized_headers
    x_amz = headers.select{|name, value| name.to_s =~ /^x-amz-/i }
    x_amz = x_amz.collect{|name, value| [name.downcase, value] }
    x_amz = x_amz.sort_by{|name, value| name }
    x_amz = x_amz.collect{|name, value| "#{name}:#{value}" }.join("\n")
    x_amz == '' ? nil : x_amz
  end

  def canonicalized_resource
    parts = [ "/#{uri.host.split('.')[0]}" ]
    parts << uri.path
    parts << "?#{uri.query}" if uri.query
    parts.join
  end
end
