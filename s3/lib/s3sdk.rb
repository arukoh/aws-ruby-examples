require 'json'
require 'logger'
require 'aws'

class S3SDK
  ENDPOINT_AP_N1 = 's3-ap-northeast-1.amazonaws.com'

  attr_reader :bucket

  def initialize bucket_name
    s3 = AWS::S3.new(
      :s3_endpoint                => ENDPOINT_AP_N1,
      :s3_multipart_max_parts     => 1000,
      :s3_multipart_threshold     => 16777216, # 16MB
      :s3_multipart_min_part_size => 5242880, #5MB
    #  :s3_server_side_encryption => :aes256,
      :logger                     => Logger.new($stdout),
      :log_level                  => :debug,
      :log_formatter              => AWS::Core::LogFormatter.debug,
      :proxy_uri                  => proxy_uri
    )
    # find or create a bucket
    @bucket = s3.buckets[bucket_name]
    s3.buckets.create(bucket_name) unless bucket.exists?
  end

  def upload_and_delete file_name
    object = bucket.objects[File.basename(file_name)]
    yield object

    if object.exists?
      # delete a file
      puts "\n(press any key to delete the object)"
      $stdin.getc
      object.delete
    end
  end

  private

  def proxy_uri
    [ENV['HTTPS_PROXY'],ENV['https_proxy'],ENV['HTTP_PROXY'],ENV['http_proxy']].compact.uniq[0]
  end
end
