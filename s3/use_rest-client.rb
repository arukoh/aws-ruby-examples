require 'json'
require 'mime/types'
require './lib/s3rest-client'

(bucket_name, file_name, metadata) = ARGV
unless bucket_name && file_name && File.exist?(file_name)
  puts "Usage: #{File.basename(__FILE__)} <BUCKET_NAME> <FILE_NAME>"
  exit 1
end

s3request = S3RestClient.new bucket_name

puts "\nUpload"
s3request.upload(file_name, File.open(file_name), {
  "date"               => Time.now.rfc822,
  "content-type"       => MIME::Types.type_for(file_name)[0],
#  "content-length"     => File.size(file_name),
  "x-amz-acl"          => "private",
  "x-amz-storage-class"=> :reduced_redundancy.to_s.upcase,
  "x-amz-meta-data"    => (JSON[File.read(metadata)] if metadata)
})

puts "\n(press any key to delete the object)"
$stdin.getc
s3request.delete file_name
