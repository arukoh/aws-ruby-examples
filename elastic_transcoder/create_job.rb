require 'bundler/setup'
require 'aws-sdk'

pipeline_id = ARGV[0] || ENV['AMAZON_ELASTIC_TRANSCODER_PIPELINE_ID']
key         = ARGV[1] || 'sample01.wmv'
preset_id   = ARGV[2] || '1351620000000-000060'

AWS.config(elastic_transcoder_endpoint: "elastictranscoder.ap-northeast-1.amazonaws.com")

client = AWS::ElasticTranscoder.new.client

puts "==== Response(read_preset) ===="
preset = client.read_preset(id: preset_id)
p preset
puts "\n"

puts "==== Response(create_job) ===="
key_prefix = key.sub(/#{File.extname(key)}$/, '')
response = client.create_job(
  pipeline_id: pipeline_id,
  input: {
    key:          key,
    frame_rate:   'auto',
    resolution:   'auto',
    aspect_ratio: 'auto',
    interlaced:   'auto',
    container:    'auto',
  },
  output: {
    key:               "#{key_prefix}/#{preset[:preset][:id]}.#{preset[:preset][:container]}",
    thumbnail_pattern: "#{key_prefix}/thumbnail_{resolution}-{count}",
    rotate:            'auto',
    preset_id:         preset_id,
  }
)
p response
