require 'bundler/setup'
require 'aws-sdk'

topic_owner = ARGV[0]
topic_name  = ARGV[1]
topic_arn   = "arn:aws:sns:ap-northeast-1:#{topic_owner}:#{topic_name}"
 
AWS.config(sns_endpoint: 'sns.ap-northeast-1.amazonaws.com')
 
topic = AWS::SNS::Topic.new(topic_arn)

message_id = topic.publish("default message",
                           subject:    "SNS message subject",
                           email:      "message sent to email endpoints",
                           email_json: "message sent to email_json endpoints",
                           http:       "message sent to http endpoints",
                           https:      "message sent to https endpoints",
                           sqs:        "message sent to sqs endpoints",
                          )

puts message_id
