require 'yaml'
require 'rubygems'
require 'aws'

DOMAIN_NAME        = 'simpledb_test'
ITEM_DATA_SET_YAML = './data_set.yaml'
ITEM_TABLE_COLUMNS = [
  ['Item Name' , 11],
  ['Title'     , 20],
  ['Author'    , 15],
  ['Year'      , 5],
  ['Pages'     , 6],
  ['Keyword'   , 20],
  ['Rating'    , 20] ]
TABLE_HR = ITEM_TABLE_COLUMNS.map {|a| ['-'*a[1], a[1]] }

def puts_table_record r, delimiter = '|'
  puts r.map{|a| a[0].ljust(a[1])[0..(a[1]-1)]}.join(delimiter)
end

def puts_query_result items
  puts case items
    when AWS::SimpleDB::ItemCollection
      items.to_a.map{|i| "  " + i.name }
    else
      "  " + items.to_s
  end
end

def proxy_uri
  [ENV['HTTPS_PROXY'],ENV['https_proxy'],ENV['HTTP_PROXY'],ENV['http_proxy']].compact.uniq[0]
end

sdb = AWS::SimpleDB.new(
  :simple_db_endpoint => 'sdb.ap-northeast-1.amazonaws.com',
  :proxy_uri          => proxy_uri
)

# get or create domain
domain = sdb.domains[DOMAIN_NAME].exists? ?
  sdb.domains[DOMAIN_NAME] : sdb.domains.create(DOMAIN_NAME)
puts "\n==== domain info ===="
puts "name: #{domain.name}"
puts "metadata: #{domain.metadata.to_h.inspect}"

# create items
puts "\n==== create items ===="
puts_table_record TABLE_HR, '+'
puts_table_record ITEM_TABLE_COLUMNS
puts_table_record TABLE_HR, '+'
YAML.load_file(ITEM_DATA_SET_YAML).each do |key, val|
  item = domain.items.create(key, val).data(:consistent_read => true).item
  puts_table_record ITEM_TABLE_COLUMNS.map {|a| [item.attributes[a[0]].values.join(','), a[1]] }
end
puts_table_record TABLE_HR, '+'

# http://docs.amazonwebservices.com/AmazonSimpleDB/latest/DeveloperGuide/SimpleQueriesSelect.html
puts "\n==== Simple Queries ===="
puts "select * from mydomain where Title = 'The Right Stuff'"
puts_query_result domain.items.select(:all).where('Title' => 'The Right Stuff')
puts "select * from mydomain where Year > '1985'"
puts_query_result domain.items.select(:all).where('Year > ?', 1985)
puts "select * from mydomain where Rating like '****%'"
puts_query_result domain.items.select(:all).where('Rating like ?', '****%')
puts "select * from mydomain where Pages < '00320'"
puts_query_result domain.items.select(:all).where('Pages < ?', '00320')

# http://docs.amazonwebservices.com/AmazonSimpleDB/latest/DeveloperGuide/RangeQueriesSelect.html
puts "\n==== Range Queries ===="
puts "select * from mydomain where Year > '1975' and Year < '2008'"
puts_query_result domain.items.select(:all).where("Year > '1975' AND Year < '2008'")
puts "select * from mydomain where Year between '1975' and '2008'"
puts_query_result domain.items.select(:all).where('Year' => 1975..2008)
puts "select * from mydomain where Rating = '***' or Rating = '*****'"
puts_query_result domain.items.select(:all).where('Rating = ? OR Rating = ?', '***', '*****')
puts "select * from mydomain where (Year > '1950' and Year < '1960') or Year like '193%' or Year = '2007'"
puts_query_result domain.items.select(:all).where(
  '(Year > ? AND Year < ?) OR Year like ? OR Year = ?','1950', '1960', '193%', '2007')

# http://docs.amazonwebservices.com/AmazonSimpleDB/latest/DeveloperGuide/RangeValueQueriesSelect.html
puts "\n==== Multi-Valued Attribute Queries ===="
puts "select * from mydomain where Rating = '4 stars' or Rating = '****'"
puts_query_result domain.items.select(:all).where('Rating = ? OR Rating = ?', '4 stars', '****')
puts "select * from mydomain where Keyword = 'Book' and Keyword = 'Hardcover'"
puts_query_result domain.items.select(:all).where('Keyword = ? AND Keyword = ?', 'Book', 'Hardcover')
puts "select * from mydomain where every(Keyword) in ('Book', 'Paperback')"
puts_query_result domain.items.select(:all).where('every(Keyword) in ?', ['Book', 'Paperback'] )

# http://docs.amazonwebservices.com/AmazonSimpleDB/latest/DeveloperGuide/MultipleAttributeQueriesSelect.html 
puts "\n==== Multiple Predicate Queries ===="
puts "select * from mydomain where Rating = '****'"
puts_query_result domain.items.select(:all).where('Rating' => '****')
puts "select * from mydomain where every(Rating) = '****'"
puts_query_result domain.items.select(:all).where('every(Rating) = ?', '****')
puts "select * from mydomain where Keyword = 'Book' intersection Keyword = 'Hardcover'"
puts_query_result domain.items.select(:all).where('Keyword = ? intersection Keyword = ?', 'Book', 'Hardcover')

# http://docs.amazonwebservices.com/AmazonSimpleDB/latest/DeveloperGuide/SortingDataSelect.html
puts "\n==== Sorting Data ===="
puts "select * from mydomain where Year < '1980' order by Year asc"
puts_query_result domain.items.select(:all).where('Year < ?', 1980).order('Year', :asc)
puts "select * from mydomain where Year < '1980' order by Year"
puts_query_result domain.items.select(:all).where('Year < ?', 1980).order('Year')
puts "select * from mydomain where Year = '2007' intersection Author is not null order by Author desc"
puts_query_result domain.items.select(:all).where(
  'Year = ? intersection Author is not null', 2007).order('Year', :desc)
puts "select * from mydomain order by Year asc"
begin; puts_query_result domain.items.select(:all).where('').order('Year', :asc); rescue => e; p e.message; end
puts "select * from mydomain where Year < '1980' order by Year limit 2"
puts_query_result domain.items.select(:all).where('Year < ?', 1980).order('Year').limit(2)

# http://docs.amazonwebservices.com/AmazonSimpleDB/latest/DeveloperGuide/CountingDataSelect.html
puts "\n==== Counting Data ===="
puts "select count(*) from mydomain where Title = 'The Right Stuff'"
puts_query_result domain.items.select(:all).where('Title' => 'The Right Stuff').count
puts "select count(*) from mydomain where Year > '1985'"
puts_query_result domain.items.select(:all).where('Year > ?', 1985).count
puts "select count(*) from mydomain limit 500"
puts_query_result domain.items.select(:all).limit(500).count
puts "select count(*) from mydomain limit 4"
puts_query_result domain.items.select(:all).limit(4).count

# delete domain
domain.delete!
