require './lib/common'

FIXTURES_DIR = File.expand_path('fixtures', File.dirname(__FILE__))

def fixture_path table_name
  "#{FIXTURES_DIR}/#{table_name}.yaml"
end

check_all_active TABLE_SCHEMA.keys

TABLE_SCHEMA.each do |name, options|
  table = DYNAMO_DB.tables[name]
  table.load_schema
  yaml = fixture_path table.name
  puts "Load fixture file: #{yaml}"
  YAML.load_file(yaml).each do |data|
    item = table.items.create(data)
    puts "  inserted #{data}"
  end
end
