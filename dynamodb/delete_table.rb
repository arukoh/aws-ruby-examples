require './lib/common'

puts "Delete table list: #{TABLE_SCHEMA.keys.inspect}"
TABLE_SCHEMA.each do |name, options|
  table = DYNAMO_DB.tables[name]
  if table.exists?
    table.delete
    print "  #{table.name} "
    wait_change_from table, :deleting rescue nil
    puts " DELETED"
  else
    puts "  #{name} NOT EXIST"
  end
end
