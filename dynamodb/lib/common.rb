require 'yaml'
require 'rubygems'
require 'aws'

def progress interval = 1
  print '.'; sleep interval
end

def wait_change_from table, status, interval = 1
  progress while table.status == status
end

def table_active? name
  DYNAMO_DB.tables[name].status == :active rescue false
end

def inactive table_names
  table_names.map{|name| table_active?(name) ? nil : name}.compact
end

def check_all_active table_names
  names = inactive table_names
  raise "#{names} NOT ACTIVE" unless names.empty?
end

def proxy_uri
    [ENV['HTTPS_PROXY'],ENV['https_proxy'],ENV['HTTP_PROXY'],ENV['http_proxy']].compact.uniq[0]
end

REGION_AP_N1 = 'dynamodb.ap-northeast-1.amazonaws.com'
DYNAMO_DB    = AWS::DynamoDB.new(:dynamo_db_endpoint => REGION_AP_N1, :proxy_uri => proxy_uri)

TABLE_SCHEMA_YAML = 'table_schema.yaml'
TABLE_SCHEMA      = YAML.load_file(File.expand_path(TABLE_SCHEMA_YAML, File.dirname(__FILE__)))
