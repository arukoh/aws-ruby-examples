# http://docs.amazonwebservices.com/amazondynamodb/latest/developerguide/SampleTablesAndData.html
require './lib/common'

READ_CAPACITY_UNITS  = 3
WRITE_CAPACITY_UNITS = 5

puts "Create table list: #{TABLE_SCHEMA.keys.inspect}"
TABLE_SCHEMA.each do |name, options|
  unless DYNAMO_DB.tables[name].exists?
    table = DYNAMO_DB.tables.create(name, READ_CAPACITY_UNITS, WRITE_CAPACITY_UNITS, options)
    print "  #{table.name} "
    wait_change_from table, :creating
    puts " CREATED"
  else
    puts "  #{name} ALREADY EXIST"
  end
end
